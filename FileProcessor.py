import os, sys, datetime
import markdown
import common

class FileProcessor:
    """Transforms markdown file into html"""
    def __init__(self, filePath, currentDirFiles):
        """constructor
        Args:
            filePath: path to a markdown file that must be converted into html.
            currentDirFiles: array of paths to all markdown files in the current folder."""
        self.currentDirFiles = currentDirFiles
        self.path = filePath
        
    def get_shifted_file(self, shift):
        """Gets path to the next file after current.
        If current file is the last file in the current folder
        then return first file."""
        currentPos = common.find_in_iterable(self.path, self.currentDirFiles)
        nextPos = currentPos + shift
        if nextPos >= len(self.currentDirFiles):
            nextPos = 0
        nextPath = self.currentDirFiles[nextPos]
        nextDirUrl = common.get_md_file_url(nextPath)
        nextTitle = common.get_title_from_path(nextPath)
        return (nextTitle, nextDirUrl)

    def process_tags(self):
        """Creates links for tags"""
        with open(self.path, encoding='utf-8') as file:
            lines = file.readlines()
            lastLine = lines[-1]
            tags = []
            if lastLine.startswith(common.TAGS_PREFIX):
                tagsString = lastLine[lastLine.index(':') + 1:]
                tags = tagsString.split(',')
                tags = map(lambda x: x.strip(), tags)
                newLines = lines[:-1]
                newLastLine = ' ' + common.TAGS_PREFIX + ':'
                for tag in tags:
                    tagUrl = common.BLOG_ROOT + '/Tags/' + \
                        common.title_to_dirName(tag)
                    newLastLine = newLastLine + \
                        ' [' + tag + '](' + tagUrl + '),'
                newLastLine = newLastLine[:-1]
                newLines.append(newLastLine)
                return ''.join(newLines)
        return ''.join(lines)

    def load_file(self):
        """Loads all info (cration time, content, etc) that is needed for transforming into html"""
        fileName = os.path.basename(self.path)
        dirName = os.path.dirname(self.path)
        title = common.get_title(fileName)
        md = self.process_tags()
        content = markdown.markdown(md, extensions=['markdown.extensions.fenced_code'])
        datetime = common.get_creation_date(self.path).strftime("%B %d, %Y")
        (nextTitle, nextUrl) = self.get_shifted_file(-1)
        (prevTitle, prevUrl) = self.get_shifted_file(1)
        (_, currentUrl) = self.get_shifted_file(0)
        return {
            'blogRoot' : common.BLOG_ROOT,
            'fileName' : fileName,
            'dirName' : dirName,
            'title' : title,
            'content' : content,
            'datetime' : datetime,
            'nextUrl' : nextUrl,
            'nextTitle' : nextTitle,
            'prevUrl' : prevUrl,
            'prevTitle' : prevTitle,
            'currentUrl' : currentUrl
            }

    def save_processed(self, body, dirName, title):
        """Saves body into dirName/title/index.html"""
        postDir = os.path.join(dirName, common.title_to_dirName(title))
        if not os.path.isdir(postDir):
            os.mkdir(postDir)
        postPath = os.path.join(postDir, 'index.html')
        html = open(postPath, 'w', encoding = 'utf-8')
        html.write(body)

    def process_file(self):
        """Transforms markdown file into html"""
        sys.stdout.write("Processing: " + self.path  + " ..." + os.linesep)
        file = self.load_file()
        dirName = file['dirName']
        title = file['title']
        templatePath = common.find_template(dirName, title)
        template = open(templatePath, encoding='utf-8').read()
        for key in file:
            template = template.replace('@' + key + '@', file[key])
        self.save_processed(template, dirName, title)
