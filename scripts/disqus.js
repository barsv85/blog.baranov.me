$(function(){
  var disqus_shortname = 'blog-baranov-me';
  $('#comments a').click(function() {
    $.ajaxSetup({cache:true});
    $.getScript('http://' + disqus_shortname + '.disqus.com/embed.js');
    $.ajaxSetup({cache:false});
    $(this).remove();
  });
  if(/\#comments/.test(location.hash)){
    $('#comments a').trigger('click');
  }
});