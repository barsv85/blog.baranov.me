import os, sys, datetime, shutil
from FileProcessor import FileProcessor
import common

def get_posts():
    """get list of filenames with posts"""
    sources = []
    for file in os.listdir(common.POSTS_DIR):
        if file.endswith(common.MD_EXT):
            fullPath = os.path.join('.', common.POSTS_DIR, file)
            sources.append(fullPath)
    return sources

def process_files(files):
    """Transforms all given markdown files to html files."""
    for file in files:
        processor = FileProcessor(file, files)
        processor.process_file()    

def process_dir(path):
    """Transforms all *.md files to html files
    in a given folder and all sub folders except folder 'posts'"""
    currentDirFiles = []
    for sub in os.listdir(path):
        subFullPath = os.path.join(path, sub)
        if os.path.isdir(subFullPath) and subFullPath != common.POSTS_DIR:
            process_dir(subFullPath)
        else:
            if (sub.endswith(common.MD_EXT) and sub not in common.IGNORED_MD):
                currentDirFiles.append(subFullPath)
    process_files(currentDirFiles)

def generate_archive(posts):
    """Generates blog posts archive in Archive.md"""
    archive = open('Archive.md', 'w', encoding='utf-8')
    for post in reversed(posts):
        title = common.get_title_from_path(post)
        url = common.get_md_file_url(post)
        archive.write(" - [" + title + "](" + url + ")\n\n")

def get_tags_from_post(post):
    """Gets list of tags in a post"""
    with open(post, encoding='utf-8') as file:
        lines = file.readlines()
        lastLine = lines[-1]
        if lastLine.startswith(common.TAGS_PREFIX):
            tagsString = lastLine[lastLine.index(':') + 1:]
            tags = tagsString.split(',')
            tags = map(lambda x: x.strip(), tags)
    return list(tags)

def get_tags_from_posts(posts):
    """Gets map {tag : count} from posts"""
    x = {}
    for post in posts:
        for tag in get_tags_from_post(post):
            if tag in x:
                x[tag].append(post)
            else:
                x.update({tag: [post]})
    return x

def generate_tags(posts):
    """Generates tags in Tags.md and Tags/%tag%.md"""
    tagMap = get_tags_from_posts(posts)
    with open('Tags.md', 'w', encoding='utf-8') as tagsFile:
        for tag in tagMap:
            tagUrl = common.title_to_dirName(tag)
            tagsFile.write('- [' + tag + '](' + tagUrl + ')\n\n')
            with open('Tags/' + tag + '.md', 'w', encoding='utf-8') as tagFile:
                for post in tagMap[tag]:
                    title = common.get_title_from_path(post)
                    postUrl = common.get_md_file_url(post)
                    tagFile.write(' - [' + title + '](' + postUrl + ')\n\n')

def generate_index(posts):
    """Generates main root index.html e.i. the main page of the blog."""
    sys.stdout.write("Generating index.html ..." + os.linesep)
    if len(posts) > 0:
        lastPost = posts[-1]
        lastPostDir = common.get_md_file_folder(lastPost)
        lastPostHtml = os.path.join(lastPostDir, 'index.html')
        indexHtml = os.path.join('.', 'index.html')
        shutil.copyfile(lastPostHtml, indexHtml)

def main(args):
    sys.stdout.write("Started..." + os.linesep)
    posts = get_posts()
    generate_tags(posts)
    process_files(posts)
    generate_archive(posts)
    process_dir(".")
    generate_index(posts)
    sys.stdout.write("Done.")
    
if __name__ == '__main__':
    main(sys.argv[1:])
